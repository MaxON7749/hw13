﻿#include <iostream>
#include <string>
#include <cmath>
using namespace std;

class Vector {
private:
	double x;
	double y;
	double z;
public:
	Vector():x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Show() {
		cout << "Coordinates:\nx= " << x << "\ny= " << y << "\nz= " << z<<endl;
	}

	double Length() {
		double summ;
		summ = sqrt(pow(x,2) + pow(y,2) + pow(z,2));
		return summ;
	}
};

int main()
{
	Vector v, v1(5,6,7);
	v.Show();
	v1.Show();
	cout << "Vector Length " << v1.Length();
}







